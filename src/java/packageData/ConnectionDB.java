
package packageData;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class ConnectionDB {
    
    public Connection OpenConnection() throws Exception{
        
        Connection con = null;
        
        try{
            Context ctx = new InitialContext();
            DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/student");
            con = ds.getConnection();
            
            
            
        }catch(SQLException | NamingException e){
            throw new Exception("There was impossible to connect: " + e.getMessage());
        }
        return con;
    }
    
    public void CloseConnection (Connection con) throws Exception{
        
        try{
            if(con != null){
                con.close();
            }
        }catch(SQLException e){
            throw new Exception("There was impossible to close the coneection: " + e.getMessage());
            
        }
    }
 }
