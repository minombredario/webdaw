package packageData;


public class Student {
    private int nia;
    private String name;
    private String surname;

    /**
     * @return the nia
     */
    public int getNia() {
        return nia;
    }

    /**
     * @param nia the nia to set
     */
    public void setNia(int nia) {
        this.nia = nia;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * @param surname the surname to set
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }
}
