
package packageData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class ManageStudent {
    
    public void insert(Connection con, Student student) throws Exception{
        PreparedStatement st = null;
        
        try{
            st = con.prepareStatement("insert into student(nia,name,surname) values (?,?,?)");
            
            st.setInt(1, student.getNia());
            st.setString(2, student.getName());
            st.setString(3, student.getSurname());
            
            st.executeUpdate();
        }catch (SQLException e){
            throw new Exception("There was a problem to insert the student " + e.getMessage());
        }finally{
            if(st != null){
                st.close();
            }
        }
    }
    
    public Student GetByNia (Connection con, int nia) throws Exception{
        Student student = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try{
            st = con.prepareStatement("select * from student where nia=" + nia);
            
            rs = st.executeQuery();
            
            while(rs.next()){
                student = new Student();
                student.setNia(rs.getInt("nia"));
                student.setName(rs.getString("name"));
                student.setSurname(rs.getString("surname"));
            }
        }catch(SQLException e){
            throw new Exception("there was a problem to get the student " + e.getMessage());
        }finally{
            if(rs != null){
                rs.close();
            }
            if(st != null){
                st.close();
            }
        }
        return student;
    }
}
