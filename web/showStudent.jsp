<%-- 
    Document   : showStudent
    Created on : 16-ene-2017, 12:28:53
    Author     : usu1601
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="student" scope="request" class="packageData.Student" />
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>The student is:</h1>
        Nia: <jsp:getProperty name="student" property="nia" /><br>
        Name: <jsp:getProperty name="student" property="name" /><br>
        Surname: <jsp:getProperty name="student" property="surname" /><br>
        <br>
        <a href="form.jsp"> Go to form</a>
    </body>
</html>
